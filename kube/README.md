# Kubernetes Deployment Instructions

1. Configure environment variables in ConfigMap at top of file.
2. Create resources (optionally in a namespace): `kubectl apply -f taskd-deploy.yaml`
3. Create a new user: `kubectl exec -it taskd-0 -- sh scripts/create_user.sh`
4. Add more clients: `kubectl exec -it taskd-0 -- sh scripts/create_client.sh`
5. Copy out client config: `kubectl cp taskd/taskd-0:/data/taskd/pki/[clientname].tar.gz [clientname].tar.gz`
