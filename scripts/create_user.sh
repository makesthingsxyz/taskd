#!/bin/sh

SCRIPTDIR=$(dirname $0)

if [ -z "$1" ]; then
    NAME="Default User"
else
    NAME="$1"
fi

cd ${TASKDDATA}/pki
taskd add org "$TASKD_ORGANIZATION"
taskd add user "$TASKD_ORGANIZATION" "$NAME" | tee -a "$NAME.id.txt"
chmod 600 "$NAME.id.txt"

. $SCRIPTDIR/create_client.sh "$NAME" "$2"
