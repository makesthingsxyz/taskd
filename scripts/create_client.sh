#!/bin/sh

if [ -z "$1" ]; then
    NAME="Default User"
else
    NAME="$1"
fi
if [ -z "$2" ]; then
    CLIENT=$(cat /dev/urandom | tr -dc 'a-z0-9' | head -c 10)
else
    CLIENT="$2"
fi



cd ${TASKDDATA}/pki

./generate.client "$CLIENT"
tar -czf "$CLIENT.tar.gz" "$NAME.id.txt" "$CLIENT.key.pem" "$CLIENT.cert.pem" "ca.cert.pem"
chmod 600 "$CLIENT.tar.gz"
rm "$CLIENT.key.pem" "$CLIENT.cert.pem"

echo "Client configuration is located at: $PWD/$CLIENT.tar.gz"
